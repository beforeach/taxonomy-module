package com.foreach.across.samples.taxonomy.application.web.viewprocessors;

import com.foreach.across.modules.bootstrapui.elements.GlyphIcon;
import com.foreach.across.modules.bootstrapui.elements.TableViewElement;
import com.foreach.across.modules.entity.registry.EntityConfiguration;
import com.foreach.across.modules.entity.registry.EntityRegistry;
import com.foreach.across.modules.entity.views.EntityView;
import com.foreach.across.modules.entity.views.bootstrapui.processors.element.EntityListActionsProcessor;
import com.foreach.across.modules.entity.views.bootstrapui.util.SortableTableBuilder;
import com.foreach.across.modules.entity.views.processors.EntityViewProcessorAdapter;
import com.foreach.across.modules.entity.views.processors.SortableTableRenderingViewProcessor;
import com.foreach.across.modules.entity.views.processors.support.ViewElementBuilderMap;
import com.foreach.across.modules.entity.views.request.EntityViewRequest;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.entity.web.links.EntityViewLinks;
import com.foreach.across.modules.spring.security.actions.AllowableAction;
import com.foreach.across.modules.spring.security.actions.AllowableActions;
import com.foreach.across.modules.web.ui.elements.support.ContainerViewElementUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders.button;
import static com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders.glyphIcon;

@Component
@RequiredArgsConstructor
public class ActionListViewProcessor extends EntityViewProcessorAdapter
{
	private final EntityRegistry entityRegistry;
	private final EntityViewLinks entityViewLinks;

	@Override
	protected void createViewElementBuilders( EntityViewRequest entityViewRequest,
	                                          EntityView entityView,
	                                          ViewElementBuilderMap builderMap ) {
		super.createViewElementBuilders( entityViewRequest, entityView, builderMap );
		SortableTableBuilder sortableTableBuilder = builderMap.get( SortableTableRenderingViewProcessor.TABLE_BUILDER,
		                                                            SortableTableBuilder.class );
		if ( sortableTableBuilder != null ) {
			sortableTableBuilder.valueRowProcessor(
					( ctx, row ) -> ContainerViewElementUtils.find( row, EntityListActionsProcessor.CELL_NAME,
					                                                TableViewElement.Cell.class ).ifPresent(
							cell -> {
								Object currentEntity = EntityViewElementUtils.currentEntity( ctx );
								EntityConfiguration<Object> entityConfiguration = entityRegistry.getEntityConfiguration(
										currentEntity );
								AllowableActions allowableActions = entityConfiguration.getAllowableActions(
										currentEntity );
								if ( allowableActions.contains( AllowableAction.READ ) ) {
									cell.addFirstChild(
											button()
													.link()
													.iconOnly( glyphIcon( GlyphIcon.EYE_OPEN ) )
													.url( entityViewLinks.linkTo( currentEntity ).toString() )
													.build( ctx ) );
								}
							} )
			);
		}
	}
}
