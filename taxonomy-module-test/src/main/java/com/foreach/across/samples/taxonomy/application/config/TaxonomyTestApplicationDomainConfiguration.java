package com.foreach.across.samples.taxonomy.application.config;

import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import com.foreach.across.samples.taxonomy.application.domain.DomainMarker;
import org.springframework.context.annotation.Configuration;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Configuration
@EnableAcrossJpaRepositories(basePackageClasses = DomainMarker.class)
public class TaxonomyTestApplicationDomainConfiguration {
}
