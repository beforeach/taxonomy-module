package com.foreach.across.samples.taxonomy.application.config;

import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.query.*;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.entity.registry.properties.MutableEntityPropertyDescriptor;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.config.TermsEntityQueryFunctionHandler;
import com.foreach.across.samples.taxonomy.application.domain.entity.EntityWithTerms;
import com.foreach.across.samples.taxonomy.application.web.viewprocessors.ActionListViewProcessor;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.util.Arrays;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Configuration
@RequiredArgsConstructor
public class GlobalEntityConfiguration implements EntityConfigurer
{
	private final ActionListViewProcessor actionListViewProcessor;
	private final ConversionService conversionService;

	@Override
	public void configure( EntitiesConfigurationBuilder entities ) {
		entities.all().listView( lvb -> lvb.viewProcessor( actionListViewProcessor ) );
		entities.withType( EntityWithTerms.class )
		        .properties( props -> props.property( "descendants" )
		                                   .readable( true )
		                                   .writable( true )
		                                   .hidden( true )
		                                   .propertyType( TypeDescriptor.valueOf( Term.class ) )
		                                   .attribute( EntityQueryConditionTranslator.class, new AncestorEntityQueryTranslator() )
		                                   .and()
		                                   .property( "descendantsExpanded" )
		                                   .readable( true )
		                                   .writable( true )
		                                   .hidden( true )
		                                   .propertyType( TypeDescriptor.valueOf( Term.class ) )
		                                   .attribute( EntityQueryConditionTranslator.class, new CollectionEntityQueryTranslator() )
		        )
				.association( cons -> cons.name("entityWithTerms.terms").parentDeleteMode(EntityAssociation.ParentDeleteMode.IGNORE))
		        .listView( lvb -> lvb.entityQueryFilter(
				        eqf -> eqf.showProperties( "descendants" )
				                  .multiValue( "descendants" )
				                  .properties( props -> props.property( "descendants" )
				                                             .attribute( ( prop, attrs ) -> {
					                                             val cvf = prop.getValueFetcher();
					                                             ( (MutableEntityPropertyDescriptor) prop )
							                                             .setValueFetcher( e -> {
								                                             Object id = cvf.getValue( e );
								                                             if ( "NULL".equals(
										                                             id ) || !( id instanceof String ) ) {
									                                             return id;
								                                             }
								                                             return conversionService.convert( id, Term.class );
							                                             } );
				                                             } ) )
		        ) );
	}

	class AncestorEntityQueryTranslator implements EntityQueryConditionTranslator
	{
		@Override
		public EntityQueryExpression translate( EntityQueryCondition condition ) {
			EQValue[] args = Arrays.stream( condition.getArguments() )
			                       .map( item -> (Term) item )
			                       .map( item -> new EQValue( item.getId().toString() ) )
			                       .toArray( EQValue[]::new );
			EQFunction eqFunction = new EQFunction( TermsEntityQueryFunctionHandler.TERMS_TREE, args );
			return new EntityQueryCondition( "descendantsExpanded", EntityQueryOps.CONTAINS, eqFunction );
		}
	}

	class CollectionEntityQueryTranslator implements EntityQueryConditionTranslator
	{
		@Override
		public EntityQueryExpression translate( EntityQueryCondition condition ) {
			Object[] args = condition.getArguments();
			EntityQuery eq = EntityQuery.or();
			Arrays.stream( args )
			      .map( item -> new EntityQueryCondition( "terms", EntityQueryOps.CONTAINS, item ) )
			      .forEach( eq::add );
			return eq;
		}
	}
}
