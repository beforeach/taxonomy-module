package com.foreach.across.samples.taxonomy.application.domain.entity;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;

public interface EntityWithTermsRepository extends IdBasedEntityJpaRepository<EntityWithTerms> {
}
