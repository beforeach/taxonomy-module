package com.foreach.across.samples.taxonomy.application.domain.entity;

import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "taxm_entity")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class EntityWithTerms extends SettableIdBasedEntity<EntityWithTerms>
{
	@Id
	@GeneratedValue(generator = "seq_taxm_entity_id")
	@GenericGenerator(
			name = "seq_taxm_entity_id",
			strategy = AcrossSequenceGenerator.STRATEGY,
			parameters = {
					@Parameter(name = "sequenceName", value = "seq_taxm_entity_id"),
					@Parameter(name = "allocationSize", value = "1")
			}
	)
	private Long id;

	@Column
	@Length(max = 255)
	private String name;

	@ManyToMany
	@JoinTable(name = "taxm_entity_term",
			joinColumns =
			@JoinColumn(name = "entity_id", referencedColumnName = "id"),
			inverseJoinColumns =
			@JoinColumn(name = "term_id", referencedColumnName = "id")
	)
	public Set<Term> terms;
}
