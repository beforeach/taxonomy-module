package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import com.foreach.across.modules.taxonomy.domain.taxonomy.Taxonomy;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "taxm_term")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Term extends SettableIdAuditableEntity<Term> {
    @Id
    @GeneratedValue(generator = "seq_taxm_term_id")
    @GenericGenerator(
            name = "seq_taxm_term_id",
            strategy = AcrossSequenceGenerator.STRATEGY,
            parameters = {
                    @Parameter(name = "sequenceName", value = "seq_taxm_term_id"),
                    @Parameter(name = "allocationSize", value = "1")
            }
    )
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "taxonomy_id")
    private Taxonomy taxonomy;

    @ManyToOne
    @JoinColumn(name = "parent_term_id")
    private Term parent;

    @Column(name = "term_key")
    @NotBlank
    @Length(max = 255)
    private String key;

    @Column(name = "label")
    @NotBlank
    @Length(max = 255)
    private String termLabel;

    public boolean hasParent() {
        return parent != null;
    }

    public String getLabel() {
        if (!this.hasParent()) {
            return termLabel;
        }
        LinkedList<String> labels = new LinkedList<>();
        labels.add(termLabel);
        Term term = this;
        while (term.hasParent()) {
            term = term.getParent();
            labels.addFirst(term.termLabel);
        }
        return String.join(" > ", labels);
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
