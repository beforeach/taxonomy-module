package com.foreach.across.modules.taxonomy.domain.taxonomy;

import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Exposed
public interface TaxonomyRepository extends IdBasedEntityJpaRepository<Taxonomy>, QueryDslPredicateExecutor<Taxonomy> {
    @Override
    List<Taxonomy> findAll(Predicate predicate);

    @Override
    List<Taxonomy> findAll(Predicate predicate, Sort sort);

    @Override
    List<Taxonomy> findAll(Predicate predicate, OrderSpecifier<?>... orderSpecifiers);

    @Override
    List<Taxonomy> findAll(OrderSpecifier<?>... orderSpecifiers);
}
