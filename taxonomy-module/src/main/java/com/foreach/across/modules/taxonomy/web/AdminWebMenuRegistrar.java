package com.foreach.across.modules.taxonomy.web;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.adminweb.menu.AdminMenuEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * Registers the admin menu items related to taxonomy.
 *
 * @author Andy Somers
 * @since 0.0.1
 */
@ConditionalOnAcrossModule(AdminWebModule.NAME)
@Component
@RequiredArgsConstructor
public class AdminWebMenuRegistrar {
    private static final String TOP_GROUP = "/taxonomy";
    private static final String ENTITY_MGMT_GROUP = "/entities/TaxonomyModule";

    private final MessageSource messageSource;

    @EventListener
    public void buildSideNavigation(AdminMenuEvent menu) {
        // root menu + top-level items
        menu.group(TOP_GROUP)
                .title(messageSource.getMessage("TaxonomyModule.adminMenu.taxonomy", new Object[0], LocaleContextHolder.getLocale()))
                .order(Ordered.HIGHEST_PRECEDENCE + 1)
                .and()
                .optionalItem(ENTITY_MGMT_GROUP + "/taxonomy").changePathTo(TOP_GROUP + "/taxonomy").order(1).and()
                .optionalItem(ENTITY_MGMT_GROUP + "/term").changePathTo(TOP_GROUP + "/term").order(3);

        // disable the entity management section
        menu.optionalItem("/entities/TaxonomyModule").disable();
    }
}
