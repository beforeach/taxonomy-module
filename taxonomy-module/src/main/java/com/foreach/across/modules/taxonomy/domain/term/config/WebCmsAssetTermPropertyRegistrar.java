package com.foreach.across.modules.taxonomy.domain.term.config;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.core.annotations.Exposed;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiElements;
import com.foreach.across.modules.bootstrapui.elements.SelectFormElementConfiguration;
import com.foreach.across.modules.entity.config.builders.EntityPropertyRegistryBuilder;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.TermWebCmsAssetService;
import com.foreach.across.modules.webcms.WebCmsModule;
import com.foreach.across.modules.webcms.domain.asset.QWebCmsAssetLink;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAsset;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAssetLink;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAssetLinkRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.foreach.across.modules.taxonomy.domain.term.TermWebCmsAssetService.TERM_TO_ASSET;

/**
 * Supports quick configuration of a link between {@link WebCmsAsset} and a {@link Term}.
 *
 * @author Andy Somers
 * @since 0.0.1
 */
@ConditionalOnAcrossModule(WebCmsModule.NAME)
@Component
@Exposed
@RequiredArgsConstructor
public class WebCmsAssetTermPropertyRegistrar {
    private final TermWebCmsAssetService termWebCmsAssetService;
    private final WebCmsAssetLinkRepository assetLinkRepository;

    @SuppressWarnings("unchecked")
    public Consumer<EntityPropertyRegistryBuilder> registerForMultipleTerms() {
        return props -> props.property("terms")
                .displayName("Terms")
                .writable(true)
                .controller(c ->
                        c.withTarget(WebCmsAsset.class, Set.class)
                                .valueFetcher(retrieveTerms())
                                .saveConsumer((asset, holder) -> {
                                    deleteExistingLinks(asset);
                                    if (holder.getNewValue() != null) {
                                        for (Term term : (Set<Term>) holder.getNewValue()) {
                                            createLink(String.valueOf(term.getId()), asset);
                                        }
                                    }
                                })
                )
                .propertyType(TypeDescriptor.collection(Set.class, TypeDescriptor.valueOf(Term.class)))
                .attribute(SelectFormElementConfiguration.class, null)
                .viewElementType(ViewElementMode.CONTROL, BootstrapUiElements.SELECT);
    }

    @SuppressWarnings("unchecked")
    public Consumer<EntityPropertyRegistryBuilder> registerForSingleTerm() {
        return props -> props.property("term")
                .displayName("Term")
                .writable(true)
                .controller(c ->
                        c.withTarget(WebCmsAsset.class, Term.class)
                                .valueFetcher(retrieveTerm())
                                .saveConsumer((asset, holder) -> {
                                    deleteExistingLinks(asset);
                                    if (holder.getNewValue() != null) {
                                        Term newValue = holder.getNewValue();
                                        createLink(String.valueOf(newValue.getId()), asset);
                                    }
                                })
                )
                .propertyType(TypeDescriptor.valueOf(Term.class))
                .attribute(SelectFormElementConfiguration.class, null)
                .viewElementType(ViewElementMode.CONTROL, BootstrapUiElements.SELECT);
    }

    private void deleteExistingLinks(WebCmsAsset asset) {
        BooleanExpression expression = QWebCmsAssetLink.webCmsAssetLink.asset.eq(asset).and(
                QWebCmsAssetLink.webCmsAssetLink.linkType.equalsIgnoreCase(TERM_TO_ASSET));

        // delete all asset links and make new ones
        Iterable<WebCmsAssetLink> currentLinksTo = assetLinkRepository.findAll(expression);
        assetLinkRepository.delete(currentLinksTo);
        assetLinkRepository.flush();
    }

    private void createLink(String objectId, WebCmsAsset asset) {
        WebCmsAssetLink link = new WebCmsAssetLink();
        link.setOwnerObjectId(objectId);
        link.setAsset(asset);
        link.setLinkType(TERM_TO_ASSET);
        assetLinkRepository.save(link);
    }

    private Function<WebCmsAsset, Set> retrieveTerms() {
        return entity -> {
            if (!entity.isNew()) {
                Set<Term> allByWebCmsAsset = termWebCmsAssetService.findAllByWebCmsAsset(entity);
                if (!allByWebCmsAsset.isEmpty()) {
                    return allByWebCmsAsset;
                }
            }
            return null;
        };
    }

    private Function<WebCmsAsset, Term> retrieveTerm() {
        return entity -> {
            if (!entity.isNew()) {
                Set<Term> allByWebCmsAsset = termWebCmsAssetService.findAllByWebCmsAsset(entity);
                if (!allByWebCmsAsset.isEmpty()) {
                    return allByWebCmsAsset.iterator().next();
                }
            }
            return null;
        };
    }
}
