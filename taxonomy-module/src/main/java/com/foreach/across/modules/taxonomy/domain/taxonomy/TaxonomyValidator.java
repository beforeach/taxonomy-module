package com.foreach.across.modules.taxonomy.domain.taxonomy;

import com.foreach.across.modules.entity.validators.EntityValidatorSupport;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
@RequiredArgsConstructor
public class TaxonomyValidator extends EntityValidatorSupport<Taxonomy> {
    private final TaxonomyRepository taxonomyRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return Taxonomy.class.isAssignableFrom(clazz);
    }

    @Override
    protected void postValidation(Taxonomy entity, Errors errors, Object... validationHints) {
        if (!errors.hasFieldErrors("key")) {
            long count = taxonomyRepository.count(QTaxonomy.taxonomy.key.equalsIgnoreCase(entity.getKey()));
            if (count > 0) {
                errors.rejectValue("key", "alreadyExists");
            }
        }
    }
}
