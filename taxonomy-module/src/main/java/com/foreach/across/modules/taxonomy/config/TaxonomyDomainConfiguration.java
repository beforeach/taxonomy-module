package com.foreach.across.modules.taxonomy.config;

import com.foreach.across.modules.hibernate.jpa.repositories.config.EnableAcrossJpaRepositories;
import com.foreach.across.modules.taxonomy.domain.DomainMarker;
import org.springframework.context.annotation.Configuration;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Configuration
@EnableAcrossJpaRepositories(basePackageClasses = DomainMarker.class)
class TaxonomyDomainConfiguration {
}
