package com.foreach.across.modules.taxonomy.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.modules.hibernate.installers.AuditableSchemaInstaller;
import org.springframework.core.annotation.Order;

import java.util.Arrays;
import java.util.Collection;


/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Order(2)
@Installer(description = "Adds auditing columns to core tables", version = 1)
public class TaxonomyAuditableInstaller extends AuditableSchemaInstaller {
    @Override
    protected Collection<String> getTableNames() {
        return Arrays.asList("taxm_taxonomy", "taxm_term");
    }
}
