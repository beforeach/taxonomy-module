package com.foreach.across.modules.taxonomy.domain.term.config;

import com.foreach.across.modules.entity.autosuggest.AutoSuggestDataEndpoint;
import com.foreach.across.modules.entity.autosuggest.AutoSuggestDataSet;
import com.foreach.across.modules.entity.conditionals.ConditionalOnAdminWeb;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.taxonomy.domain.term.QTerm;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.TermRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Configuration
@ConditionalOnAdminWeb
public class TermConfiguration implements EntityConfigurer {

    public static final String TERMS_DATA_SET = "TaxonomyModule.termsDataSet";

    @Override
    public void configure(EntitiesConfigurationBuilder entities) {
        entities.withType(Term.class)
                .association(ass -> ass.name("term.parent").hidden(true))
                .association(ass -> ass.name("termLink.term").hidden(true))
                .association(ass -> ass.name("termLink.ascendantTerm").hidden(true))
                .listView(lvb -> lvb.showProperties("taxonomy", "key", "parent", "lastModified"))
        ;
    }

    @Autowired
    public void registerDataSet(AutoSuggestDataEndpoint endpoint, TermRepository termRepository) {
        endpoint.registerDataSet(TERMS_DATA_SET,
                AutoSuggestDataSet
                        .builder()
                        .suggestionsLoader(
                                (query, controlName) -> termRepository.findAll(QTerm.term.termLabel.containsIgnoreCase(query))
                                        .stream()
                                        .map(term -> {
                                            Map<String, Object> entry = new HashMap<>();
                                            entry.put("id", term.getId());
                                            entry.put("label", term.getLabel());
                                            return entry;
                                        })
                                        .collect(Collectors.toList()))
                        .build()
        );
    }
}
