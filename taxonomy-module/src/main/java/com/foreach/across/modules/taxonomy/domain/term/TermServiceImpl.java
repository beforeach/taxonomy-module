package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.core.annotations.Exposed;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Service
@Exposed
@RequiredArgsConstructor
public class TermServiceImpl implements TermService {
    private final TermLinkRepository termLinkRepository;

    @Override
    public List<Term> getDescendants(Term term) {
        List<TermLink> all = termLinkRepository.findAll(QTermLink.termLink.ascendantTerm.eq(term));
        return all.stream().map(TermLink::getTerm).collect(Collectors.toList());
    }
}
