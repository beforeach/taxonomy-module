package com.foreach.across.modules.taxonomy.views.bootstrapui;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.entity.autosuggest.AutoSuggestDataEndpoint;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.views.EntityViewElementBuilderFactory;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.bootstrapui.processors.element.PropertyPlaceholderTextPostProcessor;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Component
@ConditionalOnAcrossModule(allOf = {AcrossHibernateJpaModule.NAME, EntityModule.NAME, AdminWebModule.NAME})
@RequiredArgsConstructor
public class TermsViewElementBuilderFactory implements EntityViewElementBuilderFactory<ViewElementBuilder> {
    public static final String TERMS_CONTROL = TermsViewElementBuilderFactory.class.getName() + "termsControl";

    private final AutoSuggestDataEndpoint endpoint;

    @Override
    public boolean supports(String viewElementType) {
        return StringUtils.equals(TERMS_CONTROL, viewElementType);
    }

    @Override
    public ViewElementBuilder createBuilder(EntityPropertyDescriptor propertyDescriptor, ViewElementMode viewElementMode, String viewElementType) {
        ViewElementMode mode = viewElementMode.forSingle();
        TermsValueViewElementBuilder termsValueViewElementBuilder = new TermsValueViewElementBuilder()
                .postProcessor(EntityViewElementUtils.controlNamePostProcessor(propertyDescriptor));
        if (ViewElementMode.VALUE.equals(mode)) {
            return termsValueViewElementBuilder;
        } else if (ViewElementMode.CONTROL.equals(mode)) {
            return new TermsFormElementBuilder(endpoint)
                    .name(propertyDescriptor.getName())
                    .postProcessor(EntityViewElementUtils.controlNamePostProcessor(propertyDescriptor))
                    .postProcessor(new PropertyPlaceholderTextPostProcessor<>());
        }
        return termsValueViewElementBuilder;
    }
}
