package com.foreach.across.modules.taxonomy.installers;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.modules.taxonomy.domain.taxonomy.QTaxonomy;
import com.foreach.across.modules.taxonomy.domain.taxonomy.Taxonomy;
import com.foreach.across.modules.taxonomy.domain.taxonomy.TaxonomyRepository;
import com.foreach.across.modules.webcms.WebCmsModule;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;

import java.util.List;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@ConditionalOnAcrossModule(WebCmsModule.NAME)
@Order(3)
@Installer(description = "Installs a default taxonomy for WebCms", phase = InstallerPhase.AfterModuleBootstrap)
@RequiredArgsConstructor
public class DefaultDataInstaller {
    private final TaxonomyRepository taxonomyRepository;

    @InstallerMethod
    public void install() {
        BooleanExpression expression = QTaxonomy.taxonomy.key.equalsIgnoreCase("default");
        List<Taxonomy> defaultTaxonomies = taxonomyRepository.findAll(expression);
        if (defaultTaxonomies.isEmpty()) {
            taxonomyRepository.save(Taxonomy.builder().name("Default taxonomy").key("default").build());
            taxonomyRepository.flush();
        }
    }
}
