package com.foreach.across.modules.taxonomy.domain.term;


import com.foreach.across.modules.hibernate.business.SettableIdBasedEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "taxm_term_link")
@AllArgsConstructor
@NoArgsConstructor
public class TermLink extends SettableIdBasedEntity<TermLink> {
    @Id
    @GeneratedValue(generator = "seq_taxm_term_link_id")
    @GenericGenerator(
            name = "seq_taxm_term_link_id",
            strategy = AcrossSequenceGenerator.STRATEGY,
            parameters = {
                    @Parameter(name = "sequenceName", value = "seq_taxm_term_link_id"),
                    @Parameter(name = "allocationSize", value = "1")
            }
    )
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "term_id")
    private Term term;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "ascendant_term_id")
    private Term ascendantTerm;

    @NotNull
    @Column
    private int depth;

    public TermLink(Term term, Term ascendantTerm, int depth) {
        this.term = term;
        this.ascendantTerm = ascendantTerm;
        this.depth = depth;
    }
}
