package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.webcms.domain.asset.WebCmsAsset;

import java.util.Set;

/**
 * Provides functionality to find {@link WebCmsAsset}s that are linked to {@link Term} entities or the other way around.
 * Flags determine whether to include the descendants of a {@link Term} or not.
 *
 * @author Andy Somers
 * @since 0.0.1
 */
public interface TermWebCmsAssetService {
    String TERM_TO_ASSET = "term-webcmsasset";

    /**
     * Finds all {@link WebCmsAsset}s linked to one {@link Term}, optionally including assets linked to its descendants
     *
     * @param term               the {@link Term} to which the {@link WebCmsAsset}s are linked
     * @param includeDescendants whether to check the descendants of the {@link Term} for linked {@link WebCmsAsset}s
     * @return {@link Set} of {@link WebCmsAsset}
     */
    Set<WebCmsAsset> findAllByTerm(Term term, boolean includeDescendants);

    /**
     * Finds all {@link Term}s linked to a {@link WebCmsAsset}
     *
     * @param asset the {@link WebCmsAsset} to which the {@link Term}s are linked
     * @return {@link  Set} of {@link Term}s
     */
    Set<Term> findAllByWebCmsAsset(WebCmsAsset asset);
}
