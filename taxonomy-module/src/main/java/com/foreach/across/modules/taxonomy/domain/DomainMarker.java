package com.foreach.across.modules.taxonomy.domain;

/**
 * A simple marker interface to use for scanning the domain models.
 *
 * @author Andy Somers
 * @since 0.0.1
 */
public interface DomainMarker {
}
