package com.foreach.across.modules.taxonomy.views.bootstrapui;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.entity.views.ViewElementTypeLookupStrategy;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

/**
 * Registers a {@link TermsValueViewElementBuilder} for {@link Term} properties.
 *
 * @author Andy Somers
 * @see TermsViewElementBuilderFactory
 * @since 0.0.1
 */
@Component
@Order(1)
@ConditionalOnAcrossModule(allOf = {AcrossHibernateJpaModule.NAME, EntityModule.NAME})
public class TermsViewElementLookupStrategy implements ViewElementTypeLookupStrategy {
    @Override
    public String findElementType(EntityPropertyDescriptor descriptor, ViewElementMode viewElementMode) {
        TypeDescriptor propertyType = descriptor.getPropertyTypeDescriptor();
        if ((viewElementMode.equals(ViewElementMode.VALUE) || viewElementMode.forSingle().equals(ViewElementMode.CONTROL))
                && propertyType.isCollection() && Term.class.isAssignableFrom(propertyType.getElementTypeDescriptor().getType())) {
            return TermsViewElementBuilderFactory.TERMS_CONTROL;
        }
        return null;
    }
}
