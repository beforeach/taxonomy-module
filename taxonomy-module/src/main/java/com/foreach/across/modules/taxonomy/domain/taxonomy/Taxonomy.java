package com.foreach.across.modules.taxonomy.domain.taxonomy;

import com.foreach.across.modules.hibernate.business.SettableIdAuditableEntity;
import com.foreach.across.modules.hibernate.id.AcrossSequenceGenerator;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Entity
@Getter
@Setter
@Table(name = "taxm_taxonomy")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Taxonomy extends SettableIdAuditableEntity<Taxonomy> {
    @Id
    @GeneratedValue(generator = "seq_taxm_taxonomy_id")
    @GenericGenerator(
            name = "seq_taxm_taxonomy_id",
            strategy = AcrossSequenceGenerator.STRATEGY,
            parameters = {
                    @Parameter(name = "sequenceName", value = "seq_taxm_taxonomy_id"),
                    @Parameter(name = "allocationSize", value = "1")
            }
    )
    private Long id;

    @Column(name = "name")
    @NotBlank
    @Length(max = 255)
    private String name;

    @Column(name = "taxonomy_key")
    @NotBlank
    @Length(max = 255)
    private String key;

}
