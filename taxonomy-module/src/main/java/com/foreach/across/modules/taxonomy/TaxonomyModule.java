package com.foreach.across.modules.taxonomy;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.core.context.configurer.ApplicationContextConfigurer;
import com.foreach.across.core.context.configurer.ComponentScanConfigurer;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.webcms.WebCmsModule;

import java.util.Set;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@AcrossDepends(required = AcrossHibernateJpaModule.NAME, optional = WebCmsModule.NAME)
public class TaxonomyModule extends AcrossModule {
    public static final String NAME = "TaxonomyModule";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    protected void registerDefaultApplicationContextConfigurers(Set<ApplicationContextConfigurer> contextConfigurers) {
        contextConfigurers.add(ComponentScanConfigurer.forAcrossModule(TaxonomyModule.class));
    }
}
