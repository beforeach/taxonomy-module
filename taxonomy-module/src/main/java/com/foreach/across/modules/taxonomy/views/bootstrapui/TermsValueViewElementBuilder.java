package com.foreach.across.modules.taxonomy.views.bootstrapui;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.ViewElementBuilderSupport;
import com.foreach.across.modules.web.ui.elements.NodeViewElement;
import com.foreach.across.modules.web.ui.elements.builder.NodeViewElementBuilder;

import java.util.Collection;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@ConditionalOnAcrossModule(EntityModule.NAME)
public class TermsValueViewElementBuilder extends ViewElementBuilderSupport<NodeViewElement, TermsValueViewElementBuilder> {
    @SuppressWarnings("unchecked")
    @Override
    protected NodeViewElement createElement(ViewElementBuilderContext builderContext) {
        Collection<Term> terms = EntityViewElementUtils.currentPropertyValue(builderContext, Collection.class);
        if (terms != null) {
            NodeViewElementBuilder ul = BootstrapUiBuilders.node("ul");
            for (Term term : terms) {
                ul.add(BootstrapUiBuilders.node("li").add(BootstrapUiBuilders.text(term.getLabel())));
            }
            return apply(ul.build(), builderContext);
        }
        NodeViewElement build = BootstrapUiBuilders.node("span").build();
        return apply(build, builderContext);
    }
}
