package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.entity.validators.EntityValidatorSupport;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.HashSet;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Component
@RequiredArgsConstructor
public class TermValidator extends EntityValidatorSupport<Term> {
    private final TermRepository termRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return Term.class.isAssignableFrom(clazz);
    }

    @Override
    protected void postValidation(Term entity, Errors errors, Object... validationHints) {
        detectCyclicDependencies(entity, errors);

        if (!errors.hasFieldErrors("key")) {
            Term parent = entity.getParent();
            BooleanExpression booleanExpression;
            if (parent == null) {
                booleanExpression = QTerm.term.parent.isNull().and(QTerm.term.key.equalsIgnoreCase(entity.getKey()));
            } else {
                booleanExpression = QTerm.term.parent.eq(parent).and(QTerm.term.key.equalsIgnoreCase(entity.getKey()));
            }

            if (!entity.isNew()) {
                booleanExpression = booleanExpression.and(QTerm.term.id.ne(entity.getId()));
            }
            long count = termRepository.count(booleanExpression);
            if (count > 0) {
                errors.rejectValue("key", "alreadyExists");
            }
        }
    }

    private void detectCyclicDependencies(Term entity, Errors errors) {
        if (!errors.hasFieldErrors("parent") && !entity.isNew()) {
            val parents = new HashSet<Long>();
            parents.add(entity.getId());
            Term parent = entity.getParent();
            while (parent != null) {
                if (!parents.add(parent.getId())) {
                    errors.rejectValue("parent", "cyclicDependency");
                    break;
                }
                parent = parent.getParent();
            }
        }
    }

}
