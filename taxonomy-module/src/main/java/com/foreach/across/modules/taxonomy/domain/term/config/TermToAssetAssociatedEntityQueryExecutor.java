package com.foreach.across.modules.taxonomy.domain.term.config;

import com.foreach.across.modules.entity.query.AssociatedEntityQueryExecutor;
import com.foreach.across.modules.entity.query.EntityQuery;
import com.foreach.across.modules.entity.util.EntityUtils;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.TermWebCmsAssetService;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAsset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

/**
 * Fetches the {@link WebCmsAsset}s related to a {@link Term}.
 * {@see WebCmsConfiguration}
 *
 * @author Andy Somers
 * @since 0.0.1
 */
public class TermToAssetAssociatedEntityQueryExecutor extends AssociatedEntityQueryExecutor<WebCmsAsset> {
    private final TermWebCmsAssetService termWebCmsAssetService;

    TermToAssetAssociatedEntityQueryExecutor(TermWebCmsAssetService termWebCmsAssetService) {
        super(null, null);
        this.termWebCmsAssetService = termWebCmsAssetService;
    }

    @Override
    public List<WebCmsAsset> findAll(Object parent, EntityQuery query) {
        Term term = (Term) parent;
        return new ArrayList<>(termWebCmsAssetService.findAllByTerm(term, false));
    }

    @Override
    public Page<WebCmsAsset> findAll(Object parent, EntityQuery query, Pageable pageable) {
        return EntityUtils.asPage(findAll(parent, query));
    }
}
