package com.foreach.across.modules.taxonomy.domain.term.config;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.entity.query.AssociatedEntityQueryExecutor;
import com.foreach.across.modules.entity.registry.EntityAssociation;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.TermWebCmsAssetService;
import com.foreach.across.modules.webcms.WebCmsModule;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAsset;
import com.foreach.across.modules.webcms.domain.asset.web.processors.WebCmsAssetListViewProcessor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Configuration
@RequiredArgsConstructor
@ConditionalOnAcrossModule(WebCmsModule.NAME)
public class WebCmsConfiguration implements EntityConfigurer {
    private final TermWebCmsAssetService termWebCmsAssetService;

    @Override
    public void configure(EntitiesConfigurationBuilder entities) {
        entities.withType(Term.class)
                .association(ab -> ab.associationType(EntityAssociation.Type.LINKED)
                        .name("Assets")
                        .targetEntityType(WebCmsAsset.class)
                        .attribute(AssociatedEntityQueryExecutor.class,
                                new TermToAssetAssociatedEntityQueryExecutor(termWebCmsAssetService))
                        .listView(lvb ->
                                lvb.showProperties("title", "articleType", "publicationDate", "lastModified")
                                        .defaultSort(new Sort(Sort.Direction.DESC, "lastModifiedDate"))
                                        .viewProcessor(new WebCmsAssetListViewProcessor()))
                );
    }
}
