package com.foreach.across.modules.taxonomy.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.installers.AcrossLiquibaseInstaller;
import org.springframework.core.annotation.Order;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Order(1)
@Installer(description = "Installs the required database tables", version = 2)
public class TaxonomySchemaInstaller extends AcrossLiquibaseInstaller {
    public TaxonomySchemaInstaller() {
        super("installers/TaxonomyModule/schema.xml");
    }
}
