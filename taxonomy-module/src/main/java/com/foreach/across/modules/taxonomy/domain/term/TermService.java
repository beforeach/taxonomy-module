package com.foreach.across.modules.taxonomy.domain.term;

import java.util.List;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public interface TermService {
    List<Term> getDescendants(Term term);
}
