package com.foreach.across.modules.taxonomy.views.bootstrapui;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.bootstrapui.elements.BootstrapUiBuilders;
import com.foreach.across.modules.bootstrapui.elements.TextboxFormElement;
import com.foreach.across.modules.bootstrapui.elements.autosuggest.AutoSuggestFormElementConfiguration;
import com.foreach.across.modules.bootstrapui.resource.BootstrapUiFormElementsWebResources;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.entity.autosuggest.AutoSuggestDataEndpoint;
import com.foreach.across.modules.entity.bind.EntityPropertyControlName;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.registry.properties.EntityPropertyHandlingType;
import com.foreach.across.modules.entity.views.util.EntityViewElementUtils;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.taxonomy.domain.term.config.TermConfiguration;
import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import com.foreach.across.modules.web.ui.ViewElementBuilderContext;
import com.foreach.across.modules.web.ui.ViewElementBuilderSupport;
import com.foreach.across.modules.web.ui.elements.ContainerViewElement;

import static com.foreach.across.modules.entity.bind.EntityPropertyControlName.forProperty;
import static com.foreach.across.modules.entity.views.util.EntityViewElementUtils.currentPropertyDescriptor;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@ConditionalOnAcrossModule(allOf = {AcrossHibernateJpaModule.NAME, EntityModule.NAME, AdminWebModule.NAME})
public class TermsFormElementBuilder extends ViewElementBuilderSupport<ContainerViewElement, TermsFormElementBuilder> {
    private final AutoSuggestDataEndpoint endpoint;

    TermsFormElementBuilder(AutoSuggestDataEndpoint endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    protected ContainerViewElement createElement(ViewElementBuilderContext viewElementBuilderContext) {
        TextboxFormElement textbox = new TextboxFormElement();
        EntityPropertyDescriptor descriptor = currentPropertyDescriptor(viewElementBuilderContext);
        EntityPropertyControlName.ForProperty controlName = forProperty(descriptor, viewElementBuilderContext);
        textbox.setCustomTemplate("th/TaxonomyModule/terms :: terms-list-control(${component})");
        textbox.setControlName(controlName.forHandlingType(EntityPropertyHandlingType.forProperty(descriptor)).toString());

        Object entity = EntityViewElementUtils.currentEntity(viewElementBuilderContext);
        textbox.setAttribute("values", descriptor.getValueFetcher().getValue(entity));

        return BootstrapUiBuilders.div().css("js-terms-autosuggest-control")
                .add(BootstrapUiBuilders.autosuggest().configuration(
                        AutoSuggestFormElementConfiguration.withDataSet(dataset -> dataset.remoteUrl(endpoint.getDataSet(TermConfiguration.TERMS_DATA_SET).suggestionsUrl()))
                ))
                .add(textbox)
                .build();
    }

    @Override
    protected void registerWebResources(WebResourceRegistry webResourceRegistry) {
        webResourceRegistry.addPackage(BootstrapUiFormElementsWebResources.NAME);
        webResourceRegistry.addWithKey(WebResource.JAVASCRIPT_PAGE_END, "taxonomy-module",
                "/static/TaxonomyModule/js/taxonomy-module.js",
                WebResource.VIEWS);

    }
}
