package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.hibernate.aop.EntityInterceptorAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TermEntityInterceptor extends EntityInterceptorAdapter<Term> {
    private final TermLinkRepository termLinkRepository;

    @Override
    public boolean handles(Class<?> entityClass) {
        return Term.class.isAssignableFrom(entityClass);
    }

    @Override
    public void afterCreate(Term entity) {
        recreateTermLinks(entity, false);
    }

    @Override
    public void afterUpdate(Term entity) {
        recreateTermLinks(entity, true);
    }

    @Override
    public void beforeDelete(Term entity) {
        List<TermLink> existing = termLinkRepository.findAll(QTermLink.termLink.term.eq(entity));
        termLinkRepository.delete(existing);
    }

    private void recreateTermLinks(Term entity, boolean checkExisting) {
        List<TermLink> existing = Collections.emptyList();
        if (checkExisting) {
            existing = termLinkRepository.findAll(
                    QTermLink.termLink.term.eq(entity));
        }

        if (entity.hasParent() || !existing.isEmpty()) {
            List<TermLink> newLinks = getNewLinks(entity);

            if (differenceInLinks(existing, newLinks)) {
                if (!existing.isEmpty()) {
                    termLinkRepository.delete(existing);
                }
                termLinkRepository.save(newLinks);

                termLinkRepository.findAll(QTermLink.termLink.ascendantTerm.eq(entity))
                        .stream().map(TermLink::getTerm)
                        .forEach(term -> recreateTermLinks(term, true));
            }
        }
    }

    private List<TermLink> getNewLinks(Term entity) {
        Term parent = entity.getParent();
        List<Term> terms = new LinkedList<>();
        while (parent != null) {
            terms.add(parent);
            parent = parent.getParent();
        }
        int depth = terms.size();
        List<TermLink> newLinks = new ArrayList<>();
        for (int i = 0; i < depth; i++) {
            newLinks.add(new TermLink(entity, terms.get(i), depth - 1 - i));
        }
        return newLinks;
    }

    private boolean differenceInLinks(List<TermLink> originalLinks, List<TermLink> newLinks) {
        Map<Integer, Term> newLinksByDepth = newLinks.stream().collect(
                Collectors.toMap(TermLink::getDepth, TermLink::getAscendantTerm));
        Map<Integer, Term> originalLinksByDepth = originalLinks.stream().collect(
                Collectors.toMap(TermLink::getDepth, TermLink::getAscendantTerm));
        return newLinksByDepth.size() != originalLinksByDepth.size() || newLinksByDepth.entrySet().stream().anyMatch(entry -> {
            Term original = originalLinksByDepth.get(entry.getKey());
            return !entry.getValue().equals(original);
        });
    }
}
