package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.hibernate.jpa.repositories.IdBasedEntityJpaRepository;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.List;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public interface TermLinkRepository extends IdBasedEntityJpaRepository<TermLink>, QueryDslPredicateExecutor<TermLink> {
    @Override
    List<TermLink> findAll(Predicate predicate);

    @Override
    List<TermLink> findAll(Predicate predicate, Sort sort);

    @Override
    List<TermLink> findAll(Predicate predicate, OrderSpecifier<?>... orders);

    @Override
    List<TermLink> findAll(OrderSpecifier<?>... orders);
}
