package com.foreach.across.modules.taxonomy.domain.term.config;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.entity.query.EQType;
import com.foreach.across.modules.entity.query.EQTypeConverter;
import com.foreach.across.modules.entity.query.EntityQueryFunctionHandler;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.TermService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Component
@ConditionalOnAcrossModule(allOf = { AcrossHibernateJpaModule.NAME, EntityModule.NAME })
@RequiredArgsConstructor
public class TermsEntityQueryFunctionHandler implements EntityQueryFunctionHandler
{
	public static final String TERMS_TREE = "termsTree";
	public static final Integer MAX_TERMS = 1000;

	private final TermService termService;

	@Override
	public boolean accepts( String functionName, TypeDescriptor desiredType ) {
		return StringUtils.equals( functionName, TERMS_TREE );
	}

	@Override
	public Object apply( String functionName, EQType[] arguments, TypeDescriptor desiredType, EQTypeConverter argumentConverter ) {
		Class entityType = desiredType.getType();
		if ( desiredType.getType().isAssignableFrom( Long.class ) ) {
			entityType = ( (MethodParameter) desiredType.getSource() ).getDeclaringClass();
		}
		Set<Term> termsToFilter = new HashSet<>();
		for ( EQType argument : arguments ) {
			Term term = (Term) argumentConverter.convert( TypeDescriptor.valueOf( Term.class ), argument );
			termsToFilter.add( term );
		}
		List<Term> termsWithDescendants = new ArrayList<>();
		Integer number = 0;
		for ( Term termToFilter : termsToFilter ) {
			if ( number >= MAX_TERMS ) {
				break;
			}
			termsWithDescendants.add( termToFilter );
			List<Term> descendants = termService.getDescendants( termToFilter );
			termsWithDescendants.addAll( descendants );
			number += descendants.size() + 1;
		}
		if ( termsWithDescendants.size() > MAX_TERMS ) {
			termsWithDescendants = termsWithDescendants.subList( 0, MAX_TERMS );
		}
		return desiredType.getType().isAssignableFrom( entityType )
				? termsWithDescendants.toArray()
				: termsWithDescendants.stream().map( Term::getId ).toArray();
	}
}
