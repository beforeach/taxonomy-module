package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.core.annotations.ConditionalOnAcrossModule;
import com.foreach.across.modules.webcms.WebCmsModule;
import com.foreach.across.modules.webcms.domain.asset.QWebCmsAssetLink;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAsset;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAssetLink;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAssetLinkRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@ConditionalOnAcrossModule(WebCmsModule.NAME)
@Service
@RequiredArgsConstructor
public class TermWebCmsAssetServiceImpl implements TermWebCmsAssetService {
    private final WebCmsAssetLinkRepository webCmsAssetLinkRepository;
    private final TermRepository termRepository;
    private final TermService termService;

    @Override
    public Set<WebCmsAsset> findAllByTerm(Term term, boolean includeDescendants) {
        if (term == null) {
            return Collections.emptySet();
        }
        Set<WebCmsAsset> result = getAssetsForTerm(term);
        if (!includeDescendants) {
            return result;
        }
        List<Term> descendants = termService.getDescendants(term);
        for (Term descendant : descendants) {
            result.addAll(getAssetsForTerm(descendant));
        }
        return result;
    }

    @Override
    public Set<Term> findAllByWebCmsAsset(WebCmsAsset asset) {
        BooleanExpression expression = QWebCmsAssetLink.webCmsAssetLink.asset.eq(asset).and(
                QWebCmsAssetLink.webCmsAssetLink.linkType.equalsIgnoreCase(TERM_TO_ASSET));
        return StreamSupport.stream(webCmsAssetLinkRepository.findAll(expression).spliterator(), false)
                .map(a -> termRepository.findOne(Long.parseLong(a.getOwnerObjectId()))).collect(Collectors.toSet());
    }

    private Set<WebCmsAsset> getAssetsForTerm(Term term) {
        Set<WebCmsAsset> result = new HashSet<>();
        List<WebCmsAssetLink> allByOwnerObjectId = webCmsAssetLinkRepository
                .findAllByOwnerObjectIdAndLinkTypeOrderBySortIndexAsc(String.valueOf(term.getId()), TERM_TO_ASSET);
        for (WebCmsAssetLink webCmsAssetLink : allByOwnerObjectId) {
            result.add(webCmsAssetLink.getAsset());
        }
        return result;
    }
}
