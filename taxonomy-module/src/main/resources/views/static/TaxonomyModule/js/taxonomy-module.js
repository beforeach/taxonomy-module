/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

$( document ).ready( function () {

    $.each( $( '.js-terms-autosuggest-control' ), function ( i, $typeahead ) {
        var container = $( this );
        var $typeaheadInstance = $( this ).find( ".js-typeahead" );
        $typeaheadInstance.on( 'typeahead:select', function ( evt, item ) {
            if ( container.find( '.js-terms-value-item input[value=' + item.id + ']' ).length === 0 ) {
                var template = container.find( '.js-terms-typeahead-template' ).clone( false );
                template.removeClass( 'hidden js-terms-typeahead-template' );
                template.addClass( 'js-terms-value-item' );

                template.find( '[data-as-property]' ).each( function ( i, node ) {
                    node.innerText = item[$( node ).attr( 'data-as-property' )];
                } );
                template.find( '[type=hidden]' ).val( item.id ).removeAttr( 'disabled' );
                container.find( 'table' ).append( template );

                template.find( 'a' ).on( 'click', function () {
                    $( this ).closest( 'tr' ).remove();
                } );
            }

            $typeaheadInstance.typeahead( 'val', '' );
        } );

        container.find( '.js-terms-value-item a' ).on( 'click', function () {
            $( this ).closest( 'tr' ).remove();
        } )
    } );

} );