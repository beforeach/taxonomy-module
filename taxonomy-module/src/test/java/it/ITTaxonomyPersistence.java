package it;

import com.foreach.across.modules.taxonomy.domain.taxonomy.Taxonomy;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public class ITTaxonomyPersistence extends AbstractTaxonomyApplicationIT {

    @Test
    public void createTaxonomy() {
        Taxonomy taxonomy = new Taxonomy();
        String name = UUID.randomUUID().toString();
        taxonomy.setName(name);
        taxonomy.setKey(name);
        Taxonomy saved = taxonomyRepository.save(taxonomy);
        taxonomyRepository.flush();

        Taxonomy fromDb = taxonomyRepository.findOne(saved.getId());
        assertEquals(name, fromDb.getName());
        assertEquals(name, fromDb.getKey());
    }

    @Test
    public void createTerm() {
        Taxonomy taxonomy = new Taxonomy();
        String name = UUID.randomUUID().toString();
        taxonomy.setName(name);
        taxonomy.setKey(name);
        taxonomy = taxonomyRepository.save(taxonomy);
        Term term = Term.builder()
                .key("key")
                .termLabel("label")
                .taxonomy(taxonomy)
                .build();
        Term saved = termRepository.save(term);
        termRepository.flush();
        Term fromDb = termRepository.findOne(saved.getId());
        assertEquals(taxonomy, fromDb.getTaxonomy());
        assertEquals("key", fromDb.getKey());
        assertEquals("label", fromDb.getLabel());
    }

    @Test
    public void createTermWithParent() {
        Taxonomy taxonomy = new Taxonomy();
        String name = UUID.randomUUID().toString();
        taxonomy.setName(name);
        taxonomy.setKey(name);
        taxonomy = taxonomyRepository.save(taxonomy);
        Term parent = Term.builder()
                .key("key")
                .termLabel("label")
                .taxonomy(taxonomy)
                .build();
        termRepository.save(parent);
        Term term = Term.builder()
                .key("childKey")
                .termLabel("childLabel")
                .parent(parent)
                .taxonomy(taxonomy)
                .build();
        Term saved = termRepository.save(term);
        termRepository.flush();
        Term fromDb = termRepository.findOne(saved.getId());
        assertEquals(taxonomy, fromDb.getTaxonomy());
        assertEquals(parent, fromDb.getParent());
        assertEquals("childKey", fromDb.getKey());
        assertEquals("label > childLabel", fromDb.getLabel());
    }
}
