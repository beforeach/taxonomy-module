package it;

import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.taxonomy.TaxonomyModule;
import com.foreach.across.modules.taxonomy.domain.taxonomy.TaxonomyRepository;
import com.foreach.across.modules.taxonomy.domain.term.TermRepository;
import com.foreach.across.test.AcrossTestConfiguration;
import com.foreach.across.test.AcrossWebAppConfiguration;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Bootstraps a simple application with TaxonomyModule.
 *
 * @author Andy Somers
 * @since 0.0.1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@AcrossWebAppConfiguration(classes = {AbstractTaxonomyApplicationIT.Config.class})
public abstract class AbstractTaxonomyApplicationIT {
    @Autowired
    protected TaxonomyRepository taxonomyRepository;

    @Autowired
    protected TermRepository termRepository;

    @AcrossTestConfiguration(modules = {TaxonomyModule.NAME, AcrossHibernateJpaModule.NAME})
    protected static class Config {
    }
}
