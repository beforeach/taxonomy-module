package it;

import com.foreach.across.modules.taxonomy.domain.taxonomy.Taxonomy;
import com.foreach.across.modules.taxonomy.domain.term.QTermLink;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.TermLink;
import com.foreach.across.modules.taxonomy.domain.term.TermLinkRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ITTermEntityInterceptor extends AbstractTaxonomyApplicationIT {
    @Autowired
    private TermLinkRepository termLinkRepository;

    @Test
    public void moveSubTreeUpdatesAllTermLinks() {
        Taxonomy taxonomy = new Taxonomy();
        String name = UUID.randomUUID().toString();
        taxonomy.setName(name);
        taxonomy.setKey(name);
        taxonomy = taxonomyRepository.save(taxonomy);
        Term root = Term.builder()
                .key("root")
                .termLabel("root")
                .taxonomy(taxonomy)
                .build();
        termRepository.save(root);

        Term firstLevel1 = Term.builder()
                .key("firstLevel1")
                .termLabel("firstLevel1")
                .taxonomy(taxonomy)
                .parent(root)
                .build();

        termRepository.save(firstLevel1);

        Term firstLevel2 = Term.builder()
                .key("firstLevel2")
                .termLabel("firstLevel2")
                .taxonomy(taxonomy)
                .parent(root)
                .build();

        termRepository.save(firstLevel2);

        Term secondLevel = Term.builder()
                .key("secondLevel")
                .termLabel("secondLevel")
                .taxonomy(taxonomy)
                .parent(firstLevel1)
                .build();

        termRepository.save(secondLevel);

        Term thirdLevel1 = Term.builder()
                .key("thirdLevel1")
                .termLabel("thirdLevel1")
                .taxonomy(taxonomy)
                .parent(secondLevel)
                .build();

        termRepository.save(thirdLevel1);

        Term thirdLevel2 = Term.builder()
                .key("thirdLevel2")
                .termLabel("thirdLevel2")
                .taxonomy(taxonomy)
                .parent(secondLevel)
                .build();

        termRepository.save(thirdLevel2);

        List<TermLink> selfLinks = termLinkRepository.findAll(QTermLink.termLink.term.eq(secondLevel));
        assertEquals(2, selfLinks.size());
        assertTermLinksContain(selfLinks, secondLevel, firstLevel1);
        assertTermLinksContain(selfLinks, secondLevel, root);
        List<TermLink> ascendantLinks = termLinkRepository.findAll(QTermLink.termLink.ascendantTerm.eq(secondLevel));
        assertEquals(2, ascendantLinks.size());
        assertTermLinksContain(ascendantLinks, thirdLevel1, secondLevel);
        assertTermLinksContain(ascendantLinks, thirdLevel2, secondLevel);

        secondLevel.setParent(firstLevel2);
        termRepository.save(secondLevel);
        termRepository.flush();
        termLinkRepository.flush();

        selfLinks = termLinkRepository.findAll(QTermLink.termLink.term.eq(secondLevel));
        assertEquals(2, selfLinks.size());
        assertTermLinksContain(selfLinks, secondLevel, firstLevel2);
        assertTermLinksContain(selfLinks, secondLevel, root);

        ascendantLinks = termLinkRepository.findAll(QTermLink.termLink.ascendantTerm.eq(secondLevel));
        assertEquals(2, ascendantLinks.size());
        assertTermLinksContain(ascendantLinks, thirdLevel1, secondLevel);
        assertTermLinksContain(ascendantLinks, thirdLevel2, secondLevel);

        List<TermLink> noDescendants = termLinkRepository.findAll(QTermLink.termLink.ascendantTerm.eq(firstLevel1));
        assertTrue(noDescendants.isEmpty());
    }

    private void assertTermLinksContain(List<TermLink> links, Term selfTerm, Term ascendantTerm) {
        boolean contains = false;
        Long selfTermId = selfTerm.getId();
        Long ascendantTermId = ascendantTerm.getId();
        for (TermLink link : links) {
            Long linkSelfTermId = link.getTerm().getId();
            Long linkAscendantTermId = link.getAscendantTerm().getId();
            contains = linkSelfTermId.equals(selfTermId) && linkAscendantTermId.equals(ascendantTermId);
            if (contains) {
                break;
            }
        }

        assertTrue(contains);
    }
}
