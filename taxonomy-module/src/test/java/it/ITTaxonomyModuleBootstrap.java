package it;

import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.taxonomy.TaxonomyModule;
import com.foreach.across.test.AcrossTestContext;
import org.junit.Test;

import static com.foreach.across.test.support.AcrossTestBuilders.web;
import static org.junit.Assert.assertNotNull;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public class ITTaxonomyModuleBootstrap {
    @Test
    public void taxonomyModule() {
        try (AcrossTestContext context = web()
                .modules(TaxonomyModule.NAME, AcrossHibernateJpaModule.NAME)
                .build()) {
            assertNotNull(context);
        }
    }
}
