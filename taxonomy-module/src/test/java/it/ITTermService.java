package it;

import com.foreach.across.modules.taxonomy.domain.taxonomy.Taxonomy;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.foreach.across.modules.taxonomy.domain.term.TermService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public class ITTermService extends AbstractTaxonomyApplicationIT {
    @Autowired
    private TermService termService;

    @Test
    public void getDescendantsOfTerm() {
        Taxonomy taxonomy = Taxonomy.builder().key("test").name("test").build();
        taxonomyRepository.save(taxonomy);

        Term root = Term.builder().taxonomy(taxonomy).key("root").termLabel("root").build();
        Term first = Term.builder().taxonomy(taxonomy).key("first").termLabel("first").parent(root).build();
        Term second1 = Term.builder().taxonomy(taxonomy).key("second1").termLabel("second1").parent(first).build();
        Term second2 = Term.builder().taxonomy(taxonomy).key("second2").termLabel("second2").parent(first).build();
        Term third = Term.builder().taxonomy(taxonomy).key("third").termLabel("third").parent(second1).build();
        termRepository.save(root);
        termRepository.save(first);
        termRepository.save(second1);
        termRepository.save(second2);
        termRepository.save(third);

        List<Term> rootDescendants = termService.getDescendants(root);
        assertEquals(4, rootDescendants.size());
        assertTrue(rootDescendants.contains(first));
        assertTrue(rootDescendants.contains(second1));
        assertTrue(rootDescendants.contains(second2));
        assertTrue(rootDescendants.contains(third));

        List<Term> secondOneDescendant = termService.getDescendants(second1);
        assertEquals(1, secondOneDescendant.size());
        assertEquals(third, secondOneDescendant.get(0));
    }
}
