package com.foreach.across.modules.taxonomy.views.bootstrapui;

import com.foreach.across.modules.entity.registry.properties.SimpleEntityPropertyDescriptor;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.web.ui.ViewElementBuilder;
import com.foreach.across.modules.web.ui.ViewElementBuilderSupport;
import org.junit.Test;

import static com.foreach.across.modules.taxonomy.views.bootstrapui.TermsViewElementBuilderFactory.TERMS_CONTROL;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public class TestTermsValueViewElementBuilderFactory {
    private TermsViewElementBuilderFactory factory = new TermsViewElementBuilderFactory(null);

    @Test
    public void supports() {
        assertThat(factory.supports(TERMS_CONTROL)).isTrue();
    }

    @Test
    public void createsFileReferenceViewElementBuilder() {
        ViewElementBuilder builder = factory.createBuilder(new SimpleEntityPropertyDescriptor("my-descriptor"), ViewElementMode.VALUE,
                TERMS_CONTROL);
        assertThat(builder)
                .isInstanceOf(TermsValueViewElementBuilder.class);
        assertThat(((ViewElementBuilderSupport) builder)).extracting("postProcessors")
                .isNotEmpty();
    }
}
