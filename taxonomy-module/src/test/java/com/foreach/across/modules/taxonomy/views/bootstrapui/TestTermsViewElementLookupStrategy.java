package com.foreach.across.modules.taxonomy.views.bootstrapui;

import com.foreach.across.modules.entity.registry.properties.EntityPropertyDescriptor;
import com.foreach.across.modules.entity.views.ViewElementMode;
import com.foreach.across.modules.taxonomy.domain.term.Term;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.convert.TypeDescriptor;

import java.util.Collection;

import static com.foreach.across.modules.taxonomy.views.bootstrapui.TermsViewElementBuilderFactory.TERMS_CONTROL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public class TestTermsViewElementLookupStrategy {
    private TermsViewElementLookupStrategy strategy = new TermsViewElementLookupStrategy();

    private EntityPropertyDescriptor singleValueDescriptor = mock(EntityPropertyDescriptor.class);
    private EntityPropertyDescriptor multiValueDescriptor = mock(EntityPropertyDescriptor.class);

    @Before
    public void setUp() {
        reset(singleValueDescriptor, multiValueDescriptor);
    }

    @Test
    public void supportsControlAndValueModes() {
        assertThat(lookup(Term.class, ViewElementMode.FORM_READ)).isNull();
        assertThat(lookup(Term.class, ViewElementMode.FORM_WRITE)).isNull();
        assertThat(lookup(Term.class, ViewElementMode.LABEL)).isNull();
        assertThat(lookup(Term.class, ViewElementMode.LIST_LABEL)).isNull();
        assertThat(lookup(Term.class, ViewElementMode.FILTER_CONTROL)).isNull();
        assertThat(lookup(Term.class, ViewElementMode.CONTROL)).isEqualTo(TERMS_CONTROL);
        assertThat(lookup(Term.class, ViewElementMode.LIST_CONTROL)).isNull();
        assertThat(lookup(Term.class, ViewElementMode.LIST_VALUE)).isNull();
        assertThat(lookup(Term.class, ViewElementMode.VALUE)).isEqualTo(TERMS_CONTROL);

        assertThat(lookup(Term.class, ViewElementMode.CONTROL.forMultiple())).isEqualTo(TERMS_CONTROL);
        assertThat(lookup(Term.class, ViewElementMode.FILTER_CONTROL.forMultiple())).isNull();
        assertThat(lookup(Term.class, ViewElementMode.VALUE.forMultiple())).isNull();
        assertThat(lookup(Term.class, ViewElementMode.LABEL.forMultiple())).isNull();
    }

    @Test
    public void onlySupportsTermsTypes() {
        assertThat(lookup(String.class, ViewElementMode.CONTROL)).isNull();
        assertThat(lookup(String.class, ViewElementMode.LIST_CONTROL)).isNull();
        assertThat(lookup((Class) null, ViewElementMode.CONTROL)).isNull();
        assertThat(lookup((Class) null, ViewElementMode.VALUE)).isNull();

        assertThat(lookup(Term.class, ViewElementMode.CONTROL)).isEqualTo(TERMS_CONTROL);
        assertThat(lookup(Term.class, ViewElementMode.VALUE)).isEqualTo(TERMS_CONTROL);

        assertThat(lookupMulti(Term.class, ViewElementMode.CONTROL)).isEqualTo(TERMS_CONTROL);
        assertThat(lookupMulti(Term.class, ViewElementMode.VALUE)).isNotNull();
        assertThat(lookupMulti(Term.class, ViewElementMode.LIST_LABEL)).isNull();
        assertThat(lookupMulti(Term.class, ViewElementMode.FORM_WRITE)).isNull();
        assertThat(lookupMulti(String.class, ViewElementMode.LIST_LABEL)).isNull();
        assertThat(lookupMulti(null, ViewElementMode.FORM_WRITE)).isNull();
    }

    @SuppressWarnings("unchecked")
    private String lookup(Class propertyType, ViewElementMode viewElementMode) {
        TypeDescriptor typeDescriptor = TypeDescriptor.valueOf(propertyType);
        when(singleValueDescriptor.getPropertyTypeDescriptor()).thenReturn(TypeDescriptor.collection(Collection.class, typeDescriptor));
        return lookup(singleValueDescriptor, viewElementMode);
    }

    @SuppressWarnings("unchecked")
    private String lookupMulti(Class propertyType, ViewElementMode viewElementMode) {
        TypeDescriptor typeDescriptor = TypeDescriptor.valueOf(propertyType);
        when(multiValueDescriptor.getPropertyTypeDescriptor()).thenReturn(TypeDescriptor.collection(Collection.class, typeDescriptor));
        return lookup(multiValueDescriptor, viewElementMode);
    }

    private String lookup(EntityPropertyDescriptor descriptor, ViewElementMode viewElementMode) {
        return strategy.findElementType(descriptor, viewElementMode);
    }
}
