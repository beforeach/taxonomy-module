package com.foreach.across.modules.taxonomy.domain.taxonomy;

import com.foreach.across.modules.taxonomy.domain.term.Term;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestTaxonomyValidator {
    @InjectMocks
    private TaxonomyValidator validator;
    @Mock
    private TaxonomyRepository taxonomyRepository;

    @Test
    public void supports() {
        assertTrue(validator.supports(Taxonomy.class));
        assertFalse(validator.supports(Term.class));
    }

    @Test
    public void validateUniquenessWhenKeyHasNoFieldErrorsYet() {
        when(taxonomyRepository.count(any(BooleanExpression.class))).thenReturn(0L);
        Taxonomy taxonomy = Taxonomy.builder().name("name").key("test").build();
        Errors errors = mock(Errors.class);
        validator.postValidation(taxonomy, errors);
        verify(errors, never()).rejectValue("key", "alreadyExists");

        when(taxonomyRepository.count(any(BooleanExpression.class))).thenReturn(1L);
        validator.postValidation(taxonomy, errors);
        verify(errors).rejectValue("key", "alreadyExists");
    }

    @Test
    public void validateUniquenessWhenKeyHasFieldErrors() {
        Taxonomy taxonomy = Taxonomy.builder().name("name").key("test").build();
        Errors errors = mock(Errors.class);
        when(errors.hasFieldErrors("key")).thenReturn(true);
        validator.postValidation(taxonomy, errors);
        verify(errors, never()).rejectValue("key", "alreadyExists");

        verify(taxonomyRepository, never()).count(any(BooleanExpression.class));
    }
}
