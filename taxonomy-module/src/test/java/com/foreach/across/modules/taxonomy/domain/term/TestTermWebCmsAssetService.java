package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.taxonomy.domain.taxonomy.Taxonomy;
import com.foreach.across.modules.webcms.domain.article.WebCmsArticle;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAsset;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAssetLink;
import com.foreach.across.modules.webcms.domain.asset.WebCmsAssetLinkRepository;
import com.querydsl.core.types.Predicate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestTermWebCmsAssetService {

    @Mock
    private WebCmsAssetLinkRepository webCmsAssetLinkRepository;
    @Mock
    private TermRepository termRepository;
    @Mock
    private TermService termService;
    @InjectMocks
    private TermWebCmsAssetServiceImpl termWebCmsAssetService;

    @Test
    public void findAllByTermForSingleTermReturnsAssets() {
        Taxonomy taxonomy = Taxonomy.builder().key("test").name("test").build();

        Term root = Term.builder().taxonomy(taxonomy).key("root").termLabel("root").id(1L).build();

        WebCmsArticle rootArticle = WebCmsArticle.builder().id(4L).build();

        List<WebCmsAssetLink> assetLinks = new ArrayList<>();
        WebCmsAssetLink assetLink = new WebCmsAssetLink();
        assetLink.setOwnerObjectId("1");
        assetLink.setAsset(rootArticle);
        assetLinks.add(assetLink);
        when(webCmsAssetLinkRepository.findAllByOwnerObjectIdAndLinkTypeOrderBySortIndexAsc(eq("1"), eq(TermWebCmsAssetService.TERM_TO_ASSET)))
                .thenReturn(assetLinks);

        Set<WebCmsAsset> result = termWebCmsAssetService.findAllByTerm(root, false);
        verify(termService, never()).getDescendants(root);
        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(rootArticle, result.iterator().next());
    }

    @Test
    public void findAllByTermForDescendantsReturnsAllAssets() {
        Taxonomy taxonomy = Taxonomy.builder().key("test").name("test").build();

        Term root = Term.builder().taxonomy(taxonomy).key("root").termLabel("root").id(1L).build();
        Term firstLevel = Term.builder().taxonomy(taxonomy).key("first").termLabel("first").id(2L).build();

        WebCmsArticle rootArticle = WebCmsArticle.builder().id(4L).build();
        WebCmsArticle firstLevelArticle = WebCmsArticle.builder().id(5L).build();

        List<WebCmsAssetLink> assetLinks = new ArrayList<>();
        WebCmsAssetLink assetLink = new WebCmsAssetLink();
        assetLink.setOwnerObjectId("1");
        assetLink.setAsset(rootArticle);
        assetLinks.add(assetLink);
        when(webCmsAssetLinkRepository.findAllByOwnerObjectIdAndLinkTypeOrderBySortIndexAsc(eq("1"), eq(TermWebCmsAssetService.TERM_TO_ASSET)))
                .thenReturn(assetLinks);

        List<WebCmsAssetLink> firstAssetLinks = new ArrayList<>();
        WebCmsAssetLink firstAssetLink = new WebCmsAssetLink();
        firstAssetLink.setOwnerObjectId("2");
        firstAssetLink.setAsset(firstLevelArticle);
        firstAssetLinks.add(firstAssetLink);
        when(webCmsAssetLinkRepository.findAllByOwnerObjectIdAndLinkTypeOrderBySortIndexAsc(eq("2"), eq(TermWebCmsAssetService.TERM_TO_ASSET)))
                .thenReturn(firstAssetLinks);

        when(termService.getDescendants(root)).thenReturn(Collections.singletonList(firstLevel));
        Set<WebCmsAsset> result = termWebCmsAssetService.findAllByTerm(root, true);
        verify(termService).getDescendants(root);
        assertNotNull(result);
        assertEquals(2, result.size());
        Iterator<WebCmsAsset> iterator = result.iterator();
        assertEquals(rootArticle, iterator.next());
        assertEquals(firstLevelArticle, iterator.next());
    }

    @Test
    public void findAllByTermWithNullTermReturnsEmptySet() {
        Set<WebCmsAsset> result = termWebCmsAssetService.findAllByTerm(null, false);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void findAllByWebCmsAssetsCallsAssetLinkRepository() {
        WebCmsArticle asset = WebCmsArticle.builder().id(4L).build();
        List<WebCmsAssetLink> assetLinks = new ArrayList<>();
        when(webCmsAssetLinkRepository.findAll(any(Predicate.class))).thenReturn(assetLinks);
        termWebCmsAssetService.findAllByWebCmsAsset(asset);
        verify(webCmsAssetLinkRepository).findAll(isA(Predicate.class));
    }

    @Test
    public void findAllByWebCmsAssetsFetchesTerms() {
        Term term = new Term();
        term.setId(1L);
        when(termRepository.findOne(1L)).thenReturn(term);
        WebCmsArticle asset = WebCmsArticle.builder().id(4L).build();
        List<WebCmsAssetLink> assetLinks = new ArrayList<>();
        WebCmsAssetLink assetLink = new WebCmsAssetLink();
        assetLink.setOwnerObjectId("1");
        assetLinks.add(assetLink);
        when(webCmsAssetLinkRepository.findAll(any(Predicate.class))).thenReturn(assetLinks);
        Set<Term> allByWebCmsAsset = termWebCmsAssetService.findAllByWebCmsAsset(asset);
        verify(webCmsAssetLinkRepository).findAll(isA(Predicate.class));
        assertEquals(1, allByWebCmsAsset.size());
        assertEquals(term, allByWebCmsAsset.iterator().next());
    }
}
