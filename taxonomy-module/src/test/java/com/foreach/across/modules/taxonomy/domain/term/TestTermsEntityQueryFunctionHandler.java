package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.entity.query.EQType;
import com.foreach.across.modules.entity.query.EQTypeConverter;
import com.foreach.across.modules.entity.query.EQValue;
import com.foreach.across.modules.taxonomy.domain.term.config.TermsEntityQueryFunctionHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.MethodParameter;
import org.springframework.core.convert.TypeDescriptor;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestTermsEntityQueryFunctionHandler {
    @InjectMocks
    private TermsEntityQueryFunctionHandler handler;
    @Mock
    private TermService termService;

    @Test
    public void acceptsHandlesFunction() {
        assertFalse(handler.accepts("bla", null));
        assertTrue(handler.accepts(TermsEntityQueryFunctionHandler.TERMS_TREE, null));
    }

    @Test
    public void applyIsLimitedTo1000() {
        EQTypeConverter converter = mock(EQTypeConverter.class);
        Term parent = Term.builder().termLabel("root").key("root").build();
        List<Term> children = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            children.add(Term.builder().parent(parent).termLabel("term" + i).key("term" + i).build());
        }
        TypeDescriptor termTypeDescriptor = TypeDescriptor.valueOf(Term.class);
        when(termService.getDescendants(parent)).thenReturn(children);
        EQValue eqValue = new EQValue("1");
        EQType[] arguments = new EQType[]{eqValue};
        when(converter.convert(termTypeDescriptor, eqValue)).thenReturn(parent);
        Object result = handler.apply(TermsEntityQueryFunctionHandler.TERMS_TREE, arguments, termTypeDescriptor, converter);
        assertEquals(1000, ((Object[]) result).length);
    }

    @Test
    public void applyWorksForTerms() {
        EQTypeConverter converter = mock(EQTypeConverter.class);
        Term parent = Term.builder().termLabel("root").key("root").build();
        TypeDescriptor termTypeDescriptor = TypeDescriptor.valueOf(Term.class);
        when(termService.getDescendants(parent)).thenReturn(new ArrayList<>());
        EQValue eqValue = new EQValue("1");
        EQType[] arguments = new EQType[]{eqValue};
        when(converter.convert(termTypeDescriptor, eqValue)).thenReturn(parent);
        Object result = handler.apply(TermsEntityQueryFunctionHandler.TERMS_TREE, arguments, termTypeDescriptor, converter);
        assertEquals(1, ((Object[]) result).length);
        assertTrue(((Object[]) result)[0] instanceof Term);
    }

    @Test
    public void applyWorksForIds() {
        EQTypeConverter converter = mock(EQTypeConverter.class);
        Term parent = Term.builder().id(1L).termLabel("root").key("root").build();
        TypeDescriptor termTypeDescriptor = mock(TypeDescriptor.class);
        doReturn(Long.class).when(termTypeDescriptor).getType();
        MethodParameter methodParameter = mock(MethodParameter.class);
        doReturn(methodParameter).when(termTypeDescriptor).getSource();
        doReturn(Term.class).when(methodParameter).getDeclaringClass();
        when(termService.getDescendants(parent)).thenReturn(new ArrayList<>());
        EQValue eqValue = new EQValue("1");
        EQType[] arguments = new EQType[]{eqValue};
        when(converter.convert(any(TypeDescriptor.class), eq(eqValue))).thenReturn(parent);
        Object result = handler.apply(TermsEntityQueryFunctionHandler.TERMS_TREE, arguments, termTypeDescriptor, converter);
        assertEquals(1, ((Object[]) result).length);
        assertTrue(((Object[]) result)[0] instanceof Long);
        assertEquals(1L, ((Object[]) result)[0]);
    }
}
