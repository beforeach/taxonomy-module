package com.foreach.across.modules.taxonomy.domain.term;

import com.foreach.across.modules.taxonomy.domain.taxonomy.Taxonomy;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestTermValidator {
    @InjectMocks
    private TermValidator termValidator;
    @Mock
    private TermRepository termRepository;

    @Test
    public void supports() {
        assertTrue(termValidator.supports(Term.class));
        assertFalse(termValidator.supports(Taxonomy.class));
    }

    @Test
    public void cyclicDependencyWhenParentHasNoFieldErrors() {
        Errors errors = mock(Errors.class);
        Taxonomy tax = Taxonomy.builder().name("tax").build();
        Term root = Term.builder().taxonomy(tax).key("test").build();
        Term first = Term.builder().taxonomy(tax).key("test").parent(root).build();
        root.setParent(first);
        root.setId(1L);
        when(errors.hasFieldErrors("parent")).thenReturn(false);
        termValidator.postValidation(root, errors);
        verify(errors).rejectValue("parent", "cyclicDependency");
    }

    @Test
    public void cyclicDependencyWhenEntityIsNew() {
        Errors errors = mock(Errors.class);
        Taxonomy tax = Taxonomy.builder().name("tax").build();
        Term root = Term.builder().taxonomy(tax).key("test").build();
        Term first = Term.builder().taxonomy(tax).key("test").parent(root).build();
        root.setParent(first);
        when(errors.hasFieldErrors("parent")).thenReturn(false);
        termValidator.postValidation(root, errors);
        verify(errors, never()).rejectValue("parent", "cyclicDependency");
    }

    @Test
    public void cyclicDependencyWhenParentHasFieldErrors() {
        Errors errors = mock(Errors.class);
        Taxonomy tax = Taxonomy.builder().name("tax").build();
        Term root = Term.builder().taxonomy(tax).key("test").build();
        when(errors.hasFieldErrors("parent")).thenReturn(true);
        termValidator.postValidation(root, errors);
        verify(errors, never()).rejectValue("parent", "cyclicDependency");
    }

    @Test
    public void uniqueKeyWithinParentWithoutFieldErrors() {
        Errors errors = mock(Errors.class);
        Taxonomy tax = Taxonomy.builder().name("tax").build();
        Term root = Term.builder().taxonomy(tax).key("test").build();
        Term first = Term.builder().taxonomy(tax).key("test").parent(root).build();
        root.setParent(first);
        root.setId(1L);
        when(errors.hasFieldErrors("key")).thenReturn(false);
        when(termRepository.count(any(BooleanExpression.class))).thenReturn(0L);
        termValidator.postValidation(root, errors);
        verify(errors, never()).rejectValue("key", "alreadyExists");

        when(termRepository.count(any(BooleanExpression.class))).thenReturn(1L);
        termValidator.postValidation(root, errors);
        verify(errors).rejectValue("key", "alreadyExists");
    }

    @Test
    public void uniqueKeyWithinParentWithFieldErrors() {
        Errors errors = mock(Errors.class);
        Taxonomy tax = Taxonomy.builder().name("tax").build();
        Term root = Term.builder().taxonomy(tax).key("test").build();
        Term first = Term.builder().taxonomy(tax).key("test").parent(root).build();
        root.setParent(first);
        root.setId(1L);
        when(errors.hasFieldErrors("key")).thenReturn(true);
        termValidator.postValidation(root, errors);
        verify(errors, never()).rejectValue("key", "alreadyExists");
        verify(termRepository, never()).count(any(BooleanExpression.class));
    }
}
