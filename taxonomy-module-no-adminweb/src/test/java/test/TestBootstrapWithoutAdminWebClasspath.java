package test;


import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.taxonomy.TaxonomyModule;
import com.foreach.across.modules.taxonomy.domain.term.TermEntityInterceptor;
import com.foreach.across.modules.taxonomy.web.AdminWebMenuRegistrar;
import com.foreach.across.test.AcrossTestContext;
import org.junit.Test;

import static com.foreach.across.test.support.AcrossTestBuilders.web;
import static java.lang.Thread.currentThread;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.util.ClassUtils.isPresent;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
public class TestBootstrapWithoutAdminWebClasspath {
    @Test
    public void classesShouldNotBeOnTheClassPath() {
        assertThat(isPresent("com.foreach.across.modules.adminweb.AdminWebModule", currentThread().getContextClassLoader())).isFalse();
    }

    @Test
    public void emptyBootstrap() {
        try (AcrossTestContext context = web(true)
                .property("acrossHibernate.hibernate.ddl-auto", "create-drop")
                .modules(EntityModule.NAME, TaxonomyModule.NAME)
                .build()) {
            assertThat(context.findBeanOfTypeFromModule(TaxonomyModule.NAME, TermEntityInterceptor.class)).isNotEmpty();
            assertThat(context.findBeanOfTypeFromModule(TaxonomyModule.NAME, AdminWebMenuRegistrar.class)).isEmpty();
        }
    }

}
