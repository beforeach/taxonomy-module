package com.foreach.across.samples.taxonomy;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.taxonomy.TaxonomyModule;
import com.foreach.across.modules.webcms.WebCmsModule;
import org.springframework.boot.SpringApplication;

import java.util.Collections;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@AcrossApplication(
        modules = {AdminWebModule.NAME, EntityModule.NAME, TaxonomyModule.NAME, AcrossHibernateJpaModule.NAME, WebCmsModule.NAME}
)
public class TaxonomyTestWebCmsApplication {
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(TaxonomyTestWebCmsApplication.class);
        springApplication.setDefaultProperties(Collections.singletonMap("spring.config.location", "${user.home}/dev-configs/axtaxm-test-webcms-application.yml"));
        springApplication.run(args);
    }
}
