package com.foreach.across.samples.taxonomy.application.config;

import com.foreach.across.modules.entity.config.EntityConfigurer;
import com.foreach.across.modules.entity.config.builders.EntitiesConfigurationBuilder;
import com.foreach.across.modules.taxonomy.domain.term.config.WebCmsAssetTermPropertyRegistrar;
import com.foreach.across.modules.webcms.domain.article.WebCmsArticle;
import com.foreach.across.modules.webcms.domain.page.WebCmsPage;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

/**
 * @author Andy Somers
 * @since 0.0.1
 */
@Configuration
@RequiredArgsConstructor
public class AdminUiConfiguration implements EntityConfigurer {
    private final WebCmsAssetTermPropertyRegistrar registrar;

    @Override
    public void configure(EntitiesConfigurationBuilder entities) {
        entities.withType(WebCmsArticle.class)
                .properties(registrar.registerForMultipleTerms())
                .createOrUpdateFormView(fvb -> fvb.showProperties(".", "terms"));
        entities.withType(WebCmsPage.class)
                .properties(registrar.registerForSingleTerm())
                .createOrUpdateFormView(fvb -> fvb.showProperties(".", "term"));
    }
}
